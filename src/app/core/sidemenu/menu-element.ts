export const menus = [
    {
        'name': 'Dashboard',
        'icon': 'dashboard',
        'link': '/auth/dashboard',
        'open': true,
        'chip': false,
    },
    {
        'name': 'DeviceList',
        'icon': 'list',
        'link': '/auth/dashboard',
        'open': true,
        'chip': false,
    },
    {
        'name': 'Reports',
        'icon': 'feedback',
        'link': '/auth/reports',
        'open': true,
        'chip': false,
    },
    {
        'name': 'Admin',
        'icon': 'account_circle',
        'link': '/auth/admin',
        'open': true,
        'chip': false,
    },
    {
        'name': 'RTC Admin',
        'icon': 'supervisor_account',
        'link': '/auth/superadmin',
        'open': true,
        'chip': false,  
    }
];
