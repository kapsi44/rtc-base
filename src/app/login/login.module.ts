import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { LoginComponent } from './login.component';
import { PasswordFlowComponent } from './password-flow/password-flow.component'
import { 
        MatCardModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatToolbarModule
       } from '@angular/material';

import { BASE_URL } from './login.tokens';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
    {path: '', component: PasswordFlowComponent},
  ];
@NgModule({
    imports: [
        MatCardModule,
        CommonModule,
        FlexLayoutModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        OAuthModule.forRoot({
            resourceServer: {
            allowedUrls: ['http://www.angular.at/api'],
            sendAccessToken: true
      }
    })
    ],
    declarations: [   
        LoginComponent,
        PasswordFlowComponent
    ],
    exports: [
        RouterModule
    ],
    providers: [
        { provide: BASE_URL, useValue: 'http://www.angular.at' }
    ]
})
export class LoginModule {
}
