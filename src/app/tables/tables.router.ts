import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeatureTableComponent } from './feature-table/feature-table.component';

const materialWidgetRoutes: Routes = [
  	{ path: '', component: FeatureTableComponent ,data: { animation: 'featured' }},
];

@NgModule({
  imports: [
    RouterModule.forChild(materialWidgetRoutes)
  	],
  exports: [
    RouterModule
  ]
})
export class TablesRouterModule {}